/*
 * Copyright (C) Newbyte. All rights reserved.
 *
 * This file is part of Eduprox, distributed under the zlib licence. For full
 * terms see the included LICENSE file.
 */

extern crate isahc;
extern crate warp;

mod utils;

use isahc::{prelude::*, HttpClient};
use serde::Deserialize;
use tokio::net::UnixListener;
use tokio_stream::wrappers::UnixListenerStream;
use warp::{http::Method, Filter};
use std::{env, net::Ipv4Addr, sync::Arc};

#[derive(Deserialize)]
struct ScheduleData {
	render_key: String, // Render key that now is required for some reason
	unit_guid: String, // GUID for school/organisation/et cetera
	selection_signature: String, // Signature generated from class/teacher/student/et cetera name
	black_and_white: bool, // Settings this to true will cause a black and white schedule to render
	width: u16, // Width of schedule in pixels
	height: u16, // Height of schedule in pixels
	year: u16,
	week: u8,
	day: u8 // Day of the week. 0 means entire week.
}

const ENV_ADDRESS_CFG: &str = "EDUPROX_API_ADDRESS";
const ENV_PORT_CFG: &str = "EDUPROX_API_PORT";
const ENV_SOCKET_PATH_CFG: &str = "EDUPROX_API_SOCKET_PATH"; // Socket refers to a UNIX socket

const SKOLA24_API_PATH: &str = "https://web.skola24.se/api/";

#[tokio::main]
async fn main() {
	let client = HttpClient::builder()
		.default_headers(&[
			// Without these headers the Skola24 server will error out when you request anything
			("Content-Type", "application/json"),
			("X-Requested-With", "XMLHttpRequest"),
			("X-Scope", "8a22163c-8662-4535-9050-bc5e1923df48")
		])
		.build()
		.unwrap();

	let cors = warp::cors()
		.allow_methods(&[Method::GET, Method::POST])
		.allow_header("Content-Type");

	let client_arc = Arc::new(client);

	let client_ptr = Arc::clone(&client_arc);
	let classes_path = warp::get()
		.and(warp::path!("skola24" / "v0" / "classes"))
		.map(move || {
			get_selection(&client_ptr)
		});

	let client_ptr = Arc::clone(&client_arc);
	let render_key_path = warp::get()
		.and(warp::path!("skola24" / "v0" / "render-key"))
		.map(move || {
			get_render_key(&client_ptr)
		});

	let client_ptr = Arc::clone(&client_arc);
	let schedule_path = warp::post()
		.and(warp::path!("skola24" / "v0" / "schedule"))
		.and(warp::body::json())
		.and(warp::body::content_length_limit(512))
		.map(move |schedule_data: ScheduleData| {
			get_schedule(&client_ptr, schedule_data)
		});

	let client_ptr = Arc::clone(&client_arc);
	let signature_path = warp::post()
		.and(warp::path!("skola24" / "v0" / "signature"))
		.and(warp::body::json())
		.and(warp::body::content_length_limit(256))
		.map(move |to_sign: String| {
			get_signature(&client_ptr, to_sign)
		});

	let routes = classes_path.or(render_key_path).or(schedule_path).or(signature_path).with(cors);

	match env::var(ENV_SOCKET_PATH_CFG).ok() {
		Some(socket_path) => {
			let listener = UnixListener::bind(socket_path)
				.expect(format!(
						"Something went wrong when binding to the Unix socket specified by {}",
						ENV_SOCKET_PATH_CFG
					).as_ref()
				);

			let incoming = UnixListenerStream::new(listener);

			warp::serve(routes)
				.run_incoming(incoming)
				.await;
		},
		None => {
			let port: u16 = match env::var(ENV_PORT_CFG).ok() {
					Some(port) => port.parse().expect(
						format!("{} was not a valid unsigned 16-bit integer", ENV_PORT_CFG)
							.as_ref(),
					),
					None => 3030
				};

			// TODO: Add IPv6 support

			let ipv4_address = match env::var(ENV_ADDRESS_CFG).ok() {
					Some(address) => address.parse().expect(
						format!(
							"{} was not a valid IPv4 address (IPv6 is not yet supported)",
							ENV_ADDRESS_CFG
						).as_ref()
					),
					None => Ipv4Addr::new(127, 0, 0, 1)
				};

			println!("Listening on {}:{}", ipv4_address, port);

			warp::serve(routes)
				.run((ipv4_address, port))
				.await;
		}
	}

}

fn get_render_key(client: &HttpClient) -> String {
	post(client, String::from("get/timetable/render/key"), String::from("null"))
}

fn get_selection(client: &HttpClient) -> String {
	post(
		client,
		String::from("get/timetable/selection"),
		String::from("{\"filters\":{\"class\":true,\"course\":false,\"group\":false,\
		\"period\":false,\"room\":false,\"student\":false,\"subject\":false,\"teacher\":false},\
		\"hostName\":\"linkoping.skola24.se\",\
		\"unitGuid\":\"ODUzZGRmNmMtYzdiNy1mZTA3LThlMTctNzIyNDY2Mjk1Y2I2\"}")
	)
}

fn get_schedule(
	client: &HttpClient,
	schedule_data: ScheduleData
) -> String {
	let post_data = format!("{{\"renderKey\":\"{render_key}\",\"host\":\"linkoping.skola24.se\",\
		\"unitGuid\":\"{unit_guid}\",\"startDate\":null,\"endDate\":null,\"scheduleDay\":{day},\
		\"blackAndWhite\":{black_and_white},\"width\":{schedule_width},\
		\"height\":{schedule_height},\"selectionType\":4,\"selection\":\"{selection_signature}\",\
		\"showHeader\":false,\"periodText\":\"\",\"week\":{week},\"year\":{year},\
		\"privateFreeTextMode\":false,\"privateSelectionMode\":null}}",
		render_key =			schedule_data.render_key,
		unit_guid =				schedule_data.unit_guid,
		selection_signature =	schedule_data.selection_signature,
		schedule_width = 		schedule_data.width,
		schedule_height = 		schedule_data.height,
		black_and_white =		utils::bool_to_str(schedule_data.black_and_white),
		year =					schedule_data.year,
		week = 					schedule_data.week,
		day = 					schedule_data.day
	);

	post(client, String::from("render/timetable"), post_data)
}

fn get_signature(client: &HttpClient, to_sign: String) -> String {
	post(client, String::from("encrypt/signature"), format!("{{\"signature\":\"{}\"}}", to_sign))
}

fn post(client: &HttpClient, path: String, payload: String) -> String {
	let assembled_path = format!("{}{}", SKOLA24_API_PATH, path);

	#[cfg(debug_assertions)]
	println!("Request to {} with payload:\n{}", assembled_path, payload);

	match client.post(assembled_path, payload) {
		Ok(mut response) => {
			match response.text() {
				Ok(response_text) => {
					return String::from(response_text);
				},
				Err(error) => {
					return format!("something went wrong: {}", error);
				}
			}
		},
		Err(error) => {
			return format!("something went wrong: {}", error);
		}
	}
}

