/*
 * Copyright (C) Newbyte. All rights reserved.
 *
 * This file is part of Eduprox, distributed under the zlib licence. For full
 * terms see the included LICENSE file.
 */

// This might seem stupid but I can't think of a better way to do this without
// pulling in something like serde_json, which probably would do this in a
// slower manner anyway
pub fn bool_to_str(input: bool) -> &'static str {
	if input {
		"true"
	} else {
		"false"
	}
}

